import processing.sound.*;
SoundFile file;
float x, y;// Variables de origen del Aguja
int b,c,d; //Variables para el temporizador
float a; // Variable que ayuda a mover el angulo
PImage laFoto; // Variable para imagen Baliza
float angle1 = 0.0;// angulo
float angle2 = 0.0;
float segLength =110;
boolean boton = false;
void setup(){
  size(800,800);//Dimensiones de la proyección
  x = width *0.5;// cordenada X de la aguja
  y = height *0.629; // cordenada Y de la aguja
  file = new SoundFile(this,"poom.mp3");// Audio baliza.mp3
}
void draw (){
  background(1);// Color del Fondo Negro
  textFont(createFont("Times new Roman",30));
  fill(204);//
  ellipse(400,400,500,500);
  fill(255);
  text("Velocímetro",320,40);
  fill(255,0,0);
  text("Km/h",380,590);
  fill(0);
  text("0",170,500);
  text("20",155,400);
  text("40",185,300);
  text("60",250,220);
  text("80",375,180);
  text("100",510,220);
  text("120",570,300);
  text("140",600,400);
  text("180",580,500);
  fill(0,0,255);
  rect(600,600,150,150); // Acelerador
  fill(255,0,0);
  rect(70,600,150,150);// Freno
  fill(255);
  text("Freno",100,780);
  fill(255);
  text("Acelerador",600,780);
  stroke (1); //color borde negro
  noFill (); // sin relleno
  ellipse (400,400,400,400);//elipse
  fill(1);
  strokeWeight(4);
  beginShape();
  vertex(225,500);
  vertex(570,500);
  endShape();
  fill(0,255,0);
  ellipse(400,500,10,10);
  // Comando para crear Aguja
    strokeWeight(3);
    stroke(255, 160); 
    angle1 = (a/float(width) ) * PI;
    pushMatrix();
    segment(x, y, angle1+3.15); 
    segment(segLength,0,0);
    popMatrix();
 //Condicion del Freno
  if ((mouseX>70)&&(mouseX<70+150)&&(mouseY>600)&&(mouseY<600+150 )&&(a>0)){
    boton =true;
    fill(255,0,0);
    rect(340,700,150,50);
    a=a-3;
    file.stop();
  }
  // Condicion del Acelerador
  if ((mouseX>600)&&(mouseX<600+150)&&(mouseY>600)&&(mouseY<600+150)&&(a<800)){
  boton = true ;
  a=a+1.8;
  if (b < 1){
  c = millis();
  }
b++;
  text("Tiempo ="+((millis()-c)/1000)+" [s]",400,790);
     if (((millis()-c)/1000)>=10){
    //Cargar imágen
    laFoto = loadImage("Baliza.jpg");
    image(laFoto,200,200);
    file.play();
     }
  }else{
    c=0;
    b=0;
  }
     //Condicion para soltar el acelerador
 if ((a>0)&&(a<=900)){   
     a--;
    
    
   } 

   println(b,c);
}
void segment(float x, float y, float a) {
  stroke(0,255,0);
  translate(x, y);
  rotate(a);
  line(0, 0, 100, 0);

}
void mousePressed(){
}
